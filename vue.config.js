module.exports = {
  css: {
    modules: true,
    sourceMap: true,
  },
  publicPath: process.env.NODE_ENV === 'production'
      ? '/'
      : '/'
};
