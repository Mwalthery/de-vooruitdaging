import Vue from 'vue'
import App from './App.vue'
import router from "./router"
//import './registerServiceWorker'
import PortalVue from 'portal-vue'
import store from "./store/store";
import {library} from '@fortawesome/fontawesome-svg-core'
import fal from "@fortawesome/fontawesome-pro/js/light.min";
import fab from "@fortawesome/fontawesome-pro/js/brands.min";
import falcss from "@fortawesome/fontawesome-pro/css/light.min.css";
//import axios from 'axios'
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import VueAnalytics from "vue-analytics";
import VueCookies from "vue-cookies";


//const BASE_URL = "http://localhost:8080";



library.add(fal, fab);
Vue.use(PortalVue);

//VUE analytics
const isProduction = process.env.NODE_ENV === "production";
Vue.use(VueAnalytics, {
    id: "UA-78632757-2",
    router,
    debug: {
        enabled: !isProduction,
        sendHitTask: isProduction
    }
});
//VUE COOKIES
Vue.use(VueCookies);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;
new Vue({
    router,
    store,
    render: h => h(App),
    data: { translations: "" },
    async created() {
        let lang = "nl";
        // Base Language is English
        /*let lang = "nl";
        if (this.$route && this.$route.path.indexOf("fr") > -1) {
          lang = "fr";
        } else if (this.$route && this.$route.path.indexOf("nl") > -1) {
          lang = "nl";
        }*/

        this.$store.dispatch("getTranslations", { lang: lang });
    }
}).$mount("#app");
