import Vue from "vue";
import VueRouter from "vue-router";
import Meta from 'vue-meta'
import Home from "./views/Home.vue";
import Case from "./views/Cases.vue";

Vue.use(VueRouter);
Vue.use(Meta, {
    keyName: 'metaInfo',
    attribute: 'data-vue-meta',
    ssrAttribute: 'data-vue-meta-server-rendered',
    tagIDKeyName: 'vmid',
    refreshOnceOnNavigation: true
})

const multiroutes = {
    home: {
        en: "/en",
        nl: "/",
        fr: "/fr"
    },
    cases: {
        en: "/en/cases",
        nl: "/cases",
        fr: "/fr/cases"
    }
};

export default new VueRouter({
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home,
            alias: ["/en", "/fr"],
            meta: {
                en: "/en",
                nl: "/",
                fr: "/fr"
            }
        },

        {
            path: multiroutes.cases.en,
            name: "Case",
            component: Case,
            alias: Object.values(multiroutes.cases).splice(1),
            meta: multiroutes.cases
        },

        {
            path: "*",
            name: "Other",
            component: Home
        }
    ],
    mode: "history",
    base: process.env.BASE_URL,

    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 };
    }
});

