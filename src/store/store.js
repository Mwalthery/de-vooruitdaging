import Vue from "vue";
import Vuex from "vuex";
import VueCookies from "vue-cookies";
import { initialState } from "./state";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

Vue.use(Vuex);
Vue.use(VueCookies);

const store = new Vuex.Store({
  state: initialState(),
  mutations,
  actions,
  getters
});

export default store;