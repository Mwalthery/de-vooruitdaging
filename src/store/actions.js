import { getAllTranslations } from "./_api";

const getTranslations = ({ commit }, { lang }) => {
    return new Promise((resolve, reject) => {
        getAllTranslations({ lang: lang })
            .then(result => {
                commit("handleTranslations", { translations: result, lang: lang });
                return resolve(result);
          })
            .catch(error => {
                const message = error.response.data.message;
                reject(new Error(message));
            });

    });
};

const setToggle = ({ commit, state }, newValue) => {
    commit("setToggleDayAndNight", newValue);
    return state.toggleDayAndNight;
};

const getDayOrNightState = ({ commit, state }) => {
    commit("getDayOrNightState");
    return state.ToggleDayAndNight;
};
// Export
export default {
    getTranslations,
    setToggle,
    getDayOrNightState
};
