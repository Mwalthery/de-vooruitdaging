const state = {
  lang: "nl",
  toggleDayAndNight: "DAY"
};

const mutations = {
  SET_TOGGLEDAYANDNIGHT: (state, newValue) => {
    state.toggleDayAndNight = newValue;
  }
};

const getters = {
  toggleDayAndNight: state => {
    return state.toggleDayAndNight;
  }
};

const actions = {
  setMessage: ({ commit, state }, newValue) => {
    commit("SET_TOGGLEDAYANDNIGHT", newValue);
    return state.toggleDayAndNight;
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
