const handleTranslations = (state, { lang, translations }) => {
  state.currentLanguage = lang;
  state.translations[lang] = translations;
  state.translationsLoaded = true;
};

const setToggleDayAndNight = (state, newValue) => {
  state.ToggleDayAndNight = newValue;
  localStorage.dayNightSwitch = newValue;
};
const getDayOrNightState = state => {
  if (localStorage.dayNightSwitch) {
    if (localStorage.dayNightSwitch == "true") {
      state.ToggleDayAndNight = true;
    } else {
      state.ToggleDayAndNight = false;
    }
  } else {
    let today = new Date();
    let currentHour = today.getHours();
    if (currentHour >= 18) {
      state.ToggleDayAndNight = false;
    } else if (currentHour >= 12) {
      state.ToggleDayAndNight = true;
    } else if (currentHour >= 6) {
      state.ToggleDayAndNight = true;
    } else {
      state.ToggleDayAndNight = true;
    }
    //true is for day state, false night state
  }
};

/* const calculateHeightOfWindow = (state) => {
  state.fullHeight = window.innerHeight - 40 + "px";
    //console.log(this.fullHeight);
}; */

// Export
export default {
  handleTranslations,
  getDayOrNightState,
  setToggleDayAndNight

  //calculateHeightOfWindow
};
