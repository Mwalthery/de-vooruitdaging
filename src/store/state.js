export const initialState = () => {
  return {
    ToggleDayAndNight: "",
    /*     *  TRANSLATIONS
     */
    translationsLoaded: false,
    languages: {
      nl: "nederlands"
      //en: "english",
      //fr: 'français'
    },
    currentLanguage: "en",
    translations: {}
  };
};
